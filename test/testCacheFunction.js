const cacheFunction = require("../cacheFunction")

const cb = (a,b,c) =>{
    return (a+b+c);
}

let result = cacheFunction(cb);
console.log(result(1,2,3));
console.log(result(1,2,3));
console.log(result(3,1,3));
console.log(result(2,2,4));
console.log(result(1,2,4));
console.log(result(1,1,4));