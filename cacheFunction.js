const cacheFunction = (cb) =>{
    if(!cb) return [];

    const cache = {};
    return (function(...args) {
        const argsKey = JSON.stringify(args);
        if(!cache[argsKey]) {
            cache[argsKey] = cb(...args);
        }
        return cache[argsKey];
    });

};

module.exports = cacheFunction;